# Stage 1 - the build process
FROM node:16 as build-deps

WORKDIR /app

COPY . ./

RUN echo "REACT_APP_BASEURL=${REACT_BASE_URL}" > .env

RUN yarn install && yarn build

# Stage 2 - the production environment
FROM nginxinc/nginx-unprivileged:1.21

WORKDIR /usr/share/nginx/html

COPY --from=build-deps /app/build .

USER root

RUN chown -R nginx:nginx /usr/share/nginx/html/ 

USER nginx

EXPOSE 8080

CMD ["nginx", "-g", "daemon off;"]
