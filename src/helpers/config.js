export const config = {
  baseTitle: 'David Brixel - ',
  baseUrl: process.env.REACT_APP_BASEURL || 'http://localhost:8080',

  defaultNotificationLenght: 4000,
};
