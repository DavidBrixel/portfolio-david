/*----------------------------------------------------------------------------*/
/* IMPORTS                                                                    */
/*----------------------------------------------------------------------------*/

import "./App.scss";
import "./cssClasses.scss";
import "./bulma-steps.css";
import React, { useEffect } from "react";
import { Route, Routes } from "react-router-dom";

import DesignPage from "./pages/DesignPage/DesignPage";
import DevPage from "./pages/DevPage/DevPage";
import FilmPage from "./pages/FilmPage/FilmPage";
import Homepage from "./pages/HomePage/HomePage";
import { useDispatch } from "react-redux";
import { useTitle } from "./hooks/useTitle";

/*----------------------------------------------------------------------------*/
/* App                                                                   */
/*----------------------------------------------------------------------------*/
const App = () => {
  const dispatch = useDispatch();

  useTitle("Home");
  useEffect(() => {}, [dispatch]);

  return (
    <>
      <div className="whole-page">
        <Routes>
          <Route path="*" element={<Homepage />} />
          <Route path="/DesignPage" element={<DesignPage />} />
          <Route path="/DevPage" element={<DevPage />} />
          <Route path="/FilmPage" element={<FilmPage />} />
        </Routes>
      </div>
    </>
  );
};

/*----------------------------------------------------------------------------*/
/* EXPORTS                                                                    */
/*----------------------------------------------------------------------------*/

export default App;
