/*----------------------------------------------------------------------------*/
/* IMPORTS                                                                    */
/*----------------------------------------------------------------------------*/

import "./DesignPage.scss";

import React from "react";

/*----------------------------------------------------------------------------*/
/* HOMEPAGE                                                                   */
/*----------------------------------------------------------------------------*/
const DesignPage = () => {
  return (
    <div>
      <div className="has-text-centered design-background">
        <h1 className="tittle">Website Designing</h1>
        <h2 className="subtittle">
          In the Student Cloud Lab I gained new experience about figma and
          designing Websites.
        </h2>
      </div>
      <div className="d-f fd-c">
        <div className="box design-title-box ">
          <p className="title is-2">IBM Planner Version 2</p>
        </div>
        <div className="box design-box fd-c font">
          <div className="columns">
            <div className="column is-6">
              <p className="title is-4">Figma Design</p>
              <div className="block">
                Creating the design for the "IBM-Planner" application was a
                multi-stage process that involved various challenges:
              </div>
              <div className="block">
                <strong>Usability:</strong> One of the main goals was to create
                a user interface that was intuitive and easy to navigate. This
                required careful planning and design of the layouts and menus.
              </div>
              <div className="block">
                <strong>Aesthetics:</strong> The design needed to look appealing
                and professional. This required careful selection of colors,
                fonts and other design elements.
              </div>
              <div className="block">
                <strong>Responsiveness:</strong> The application needed to look
                and function well on different devices and screen sizes. This
                required careful planning and testing.
              </div>
              <div className="block">
                The process began with the creation of individual drawings to
                visualize and test different design ideas. Once a preliminary
                design was finalized, the design was created using Figma.
                Adjustments were then made to the design based on feedback.
              </div>
            </div>

            <img
              className="column planner-img is-6"
              src="/images/ibm-planner.png"
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  );
};

/*----------------------------------------------------------------------------*/
/* EXPORTS                                                                    */
/*----------------------------------------------------------------------------*/
export default DesignPage;
