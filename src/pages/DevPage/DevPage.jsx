/*----------------------------------------------------------------------------*/
/* IMPORTS                                                                    */
/*----------------------------------------------------------------------------*/

import "./DevPage.scss";

import { Link } from "react-router-dom";
import React from "react";

/*----------------------------------------------------------------------------*/
/* HOMEPAGE                                                                   */
/*----------------------------------------------------------------------------*/
const DevPage = () => {
  return (
    <div>
      <div className="has-text-centered dev-background">
        <h1 className="tittle">Web Development</h1>
        <h2 className="coming-soon">Coming Soon...</h2>
        <Link to="/">
          <button className="button back is-link is-half has-text-weight-medium is-medium">
            Back to the Home Page
          </button>
        </Link>
      </div>
    </div>
  );
};

/*----------------------------------------------------------------------------*/
/* EXPORTS                                                                    */
/*----------------------------------------------------------------------------*/
export default DevPage;
