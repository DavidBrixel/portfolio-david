/*----------------------------------------------------------------------------*/
/* IMPORTS                                                                    */
/*----------------------------------------------------------------------------*/

import "./FilmPage.scss";

import React from "react";

/*----------------------------------------------------------------------------*/
/* HOMEPAGE                                                                   */
/*----------------------------------------------------------------------------*/
const FilmPage = () => {
  return (
    <div>
      <div className="has-text-centered film-background">
        <h1 className="tittle">Videography</h1>
        <h2 className="subtittle">
          Videography is a fun hobby of mine, where I could live out my
          creativity and try out new concepts
        </h2>
      </div>
      <div className="tile is-ancestor">
        <div className="tile is-5 is-vertical is-parent">
          <div className="tile is-child box is-flex-direction-column">
            <p className="title">Wedding Jonas & Sabrina</p>
            <p>
              It was very nice to gain new experience as a wedding filmmaker.
              There are many aspects where the shooting day can get exhausting
              but in the end it was worth it.
            </p>
          </div>
          <div className="tile is-child box is-flex-direction-column">
            <p className="title">Focused Vintage</p>
            <p>
              Focused Vintage is a vintage clothing seller, where we were
              allowed to do a shoot for the advertising of an online store. It
              was a very structured & organized shooting.
            </p>
          </div>
        </div>
        <div className="tile is-7 is-parent">
          <div className="tile is-child box is-flex-direction-column">
            <div className=" column is-half">
              <p className="title">Examples</p>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                semper diam at erat pulvinar, at pulvinar felis blandit.
                Vestibulum volutpat tellus diam, consequat gravida libero
                rhoncus ut. Morbi maximus, leo sit amet vehicula eleifend, nunc
                dui porta orci, quis semper odio felis ut quam.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

/*----------------------------------------------------------------------------*/
/* EXPORTS                                                                    */
/*----------------------------------------------------------------------------*/
export default FilmPage;
