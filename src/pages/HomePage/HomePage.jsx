/*----------------------------------------------------------------------------*/
/* IMPORTS                                                                    */
/*----------------------------------------------------------------------------*/

import "./HomePage.scss";

import { Link, Route, Routes } from "react-router-dom";
import React, { useRef } from "react";

import CVPDF from "./CV.pdf";
import DesignPage from "../DesignPage/DesignPage";
import DevPage from "../DevPage/DevPage";
import FilmPage from "../FilmPage/FilmPage";
import { ReactComponent as Instagram } from "./Assets/Instagram.svg";
import { ReactComponent as LinkedIn } from "./Assets/LinkedIn.svg";
import { ReactComponent as Mailbox } from "./Assets/mailbox.svg";
import { ReactComponent as Profile } from "./Assets/Profile.svg";
import emailjs from "@emailjs/browser";

/*----------------------------------------------------------------------------*/
/* HOMEPAGE                                                                   */
/*----------------------------------------------------------------------------*/
const HomePage = () => {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    // Überprüft, ob das Formular leer ist
    if (isFormEmpty(form.current)) {
      alert("Das Formular darf nicht leer sein.");
      return;
    }

    emailjs
      .sendForm(
        "service_5e0uom6",
        "template_b8wbgoj",
        form.current,
        "Vbn8U-gPgRSoLFTWm"
      )
      .then(
        (result) => {
          console.log(result.text);
          console.log("Nachricht gesendet");

          // Setzt das Formular zurück (leere es)
          resetForm(form.current);
        },
        (error) => {
          console.log(error.text);
        }
      );
  };

  const isFormEmpty = (formElement) => {
    const formInputs = formElement.querySelectorAll("input, textarea");

    for (const input of formInputs) {
      if (input.value.trim() === "") {
        return true;
      }
    }

    return false;
  };

  const resetForm = (formElement) => {
    // Setzt das Formular zurück
    formElement.reset();
  };
  return (
    <div>
      <div className="top">
        <div className="box leftMenu">
          <div className="leftMenu-section mt-5">
            <div className="center-items">
              <Profile />
            </div>
            <div className="name-job ">
              <p className="subtitle is-4 center-items">David Brixel</p>
              <p className="subtitle is-5 center-items">IT-Student</p>
            </div>
            <div className="d-f fd-r f-jc">
              <a
                href="https://ch.linkedin.com/in/david-brixel-68a233228"
                target="_blank"
                rel="noreferrer"
              >
                <LinkedIn className="mr-3" />
              </a>
              <a
                href="https://www.instagram.com/david_brixel/"
                target="_blank"
                rel="noreferrer"
              >
                <Instagram className="ml-3" />
              </a>
            </div>
          </div>
          <div>
            <hr />
          </div>
          <div className="box information">
            <div className="age">
              <p>Age:</p>
              <p>18</p>
            </div>
            <div className="infos">
              <p>Canton:</p>
              <p>Zurich</p>
            </div>
            <div className="infos">
              <p>Email:</p>
              <p>dbrixel@outlook.com</p>
            </div>
            <div className="infos">
              <p>Phone:</p>
              <p>+41 079 537 51 68</p>
            </div>
            <div className="adress">
              <p>Adress:</p>
              <p>Flaach, Switzerland</p>
            </div>
          </div>
          <div>
            <hr />
          </div>
          <div className="leftMenu-section">
            <p className="Language-title">Languages</p>
            <div className="attribute-percent">
              <p className="language">German</p>
              <p className="percent">100%</p>
            </div>
            <progress
              className="progress is-small"
              value="80"
              max="80"
            ></progress>
            <div className="attribute-percent">
              <p className="language">English</p>
              <p className="percent">75%</p>
            </div>
            <progress
              className="progress is-small"
              value="75"
              max="100"
            ></progress>
          </div>
          <div>
            <hr />
          </div>
          <div className="leftMenu-section">
            <p className="Language-title">Skills</p>
            <div className="attribute-percent">
              <p className="language">Docker</p>
              <p className="percent">80%</p>
            </div>
            <progress
              className="progress is-small"
              value="80"
              max="100"
            ></progress>
            <div className="attribute-percent">
              <p className="language">Kubernetes</p>
              <p className="percent">70%</p>
            </div>
            <progress
              className="progress is-small"
              value="70"
              max="100"
            ></progress>
            <div className="attribute-percent">
              <p className="language">HTML</p>
              <p className="percent">85%</p>
            </div>
            <progress
              className="progress is-small"
              value="85"
              max="100"
            ></progress>
            <div className="attribute-percent">
              <p className="language">Javascript</p>
              <p className="percent">50%</p>
            </div>
            <progress
              className="progress is-small"
              value="50"
              max="100"
            ></progress>
            <div className="attribute-percent">
              <p className="language">Network Infastructure</p>
              <p className="percent">75%</p>
            </div>
            <progress
              className="progress is-small"
              value="75"
              max="100"
            ></progress>
          </div>
          <div>
            <hr />
          </div>
          <div className="leftMenu-section">
            <a
              className="cv-link"
              href={CVPDF}
              download="CV David Brixel"
              target="_blank"
              rel="noopener noreferrer"
            >
              <button className="button submit bcol-orange is-link is-fullwidth has-text-weight-medium is-medium mb-5">
                Download CV
                <i className="fa-solid fa-file-arrow-down ml-3"></i>
              </button>
            </a>
          </div>
        </div>
        <div className="rightMenu ">
          <div className="box bg white-bg">
            <div className=" rightMenu-top">
              <div className="text">
                <h1 className="name-title">I'm David Brixel</h1>
                <p className="job-title">IT-Student</p>
                <p className="description">
                  Platform Developer EFZ in the third year at IBM Switzerland
                </p>
              </div>
              <div>
                <img className="profile-img" src="/images/Links-1.png" alt="" />
              </div>
            </div>
          </div>
          <div className="projects">
            <p className="projects-title">My Projects</p>
            <p className="projects-description">
              As a Platform Developer I worked on several projects in which I
              gathered new skills. Also in privat one of my hobbies is
              videography, where I did some Jobs.
            </p>
            <div className="d-f f-jc pl-4 pr-4">
              <Link to="/DevPage" className="projects-link-dev">
                <button className="button submit is-link box projects-buttons projects-button-dev">
                  <p className="button-title">Web Development</p>
                  <div className="learn-more-div">
                    <p className="learn-more">Learn More</p>
                    <i class="fa-solid fa-angle-right fa-2xs ml-3 pt-4"></i>
                  </div>
                </button>
              </Link>
              <Routes>
                <Route path="/DevPage" element={<DevPage />} />
              </Routes>
              <Link to="/DesignPage" className="projects-link-des">
                <button className="button submit is-link box projects-buttons projects-button-des">
                  <p className="button-title">Web Design</p>
                  <div className="learn-more-div">
                    <p className="learn-more">Learn More</p>
                    <i class="fa-solid fa-angle-right fa-2xs ml-3 pt-4"></i>
                  </div>
                </button>
              </Link>
              <Routes>
                <Route path="/DesignPage" element={<DesignPage />} />
              </Routes>
              <Link to="/FilmPage" className="projects-link-vid">
                <button className="button submit is-link box projects-buttons projects-button-vid">
                  <p className="button-title">Videography</p>
                  <div className="learn-more-div">
                    <p className="learn-more">Learn More</p>
                    <i class="fa-solid fa-angle-right fa-2xs ml-3 pt-4"></i>
                  </div>
                </button>
              </Link>
              <Routes>
                <Route path="/FilmPage" element={<FilmPage />} />
              </Routes>
            </div>
          </div>
        </div>
      </div>

      <div className="edu-background">
        <div className="education">
          <p className="education-title">Education</p>
          <p className="education-description">
            A school career is a fascinating journey that lays the foundation
            for personal growth and intellectual development. It is not just a
            series of lessons and exams, but also a living process that shapes
            the individual and prepares them for the many facets of life.
          </p>
          <ul className="steps stepss is-vertical">
            <li className="steps-segment stepss-segment">
              <span href="#" className="steps-marker stepss-marker"></span>
              <div className="steps-content stepss-content">
                <div className="box mb-5 columns">
                  <div className="ml-6 column is-3 ">
                    <p className="edu-title">
                      Primary & Secondary School Flaach
                    </p>
                    <div className="d-f fd-r mt-5 f-ac">
                      <p className="education-role">Student</p>
                      <p className="time-span">Aug 2012 - July 2021</p>
                    </div>
                  </div>
                  <div className="linie-vertikal"></div>
                  <div className="graduation column is-7">
                    <p className="graduation-title">School Graduation</p>
                    <p className="graduation-description">
                      During my time as a student at the primary school in
                      Flaach, I learned many basics for my later education.
                    </p>
                  </div>
                </div>
              </div>
            </li>
            <li className="steps-segment stepss-segment">
              <span href="#" className="steps-marker stepss-marker"></span>
              <div className="steps-content stepss-content">
                <div className="box mb-5 columns">
                  <div className="ml-6 column is-3">
                    <p className="edu-title">Berufsmaturitätsschule Zurich</p>
                    <div className="d-f fd-r mt-5 f-ac">
                      <p className="education-role">Student</p>
                      <p className="time-span">Aug 2021 - Today</p>
                    </div>
                  </div>
                  <div className="linie-vertikal"></div>
                  <div className="graduation column is-7">
                    <p className="graduation-title">School Graduation</p>
                    <p className="graduation-description">
                      At the moment I'm doing my Berufsmatura alongside my
                      apprenticeship so that I can go to university.
                    </p>
                  </div>
                </div>
              </div>
            </li>
            <li className="steps-segment">
              <span className="steps-marker stepss-marker"></span>
              <div className="steps-content stepss-content">
                <div className="box letzte columns">
                  <div className="ml-6 column is-3">
                    <p className="edu-title">Technische Berufsschule Zurich</p>
                    <div className="d-f fd-r mt-5 f-ac">
                      <p className="education-role">Student</p>
                      <p className="time-span">Aug 2021 - Today</p>
                    </div>
                  </div>
                  <div className="linie-vertikal"></div>
                  <div className="graduation column is-7">
                    <p className="graduation-title">School Graduation</p>
                    <p className="graduation-description">
                      Besides being 3 Days at the office I am visiting the TBZ.
                      In this school I got to know more areas in IT like
                      Networking, Container, Backup Solutions, etc.
                    </p>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <div className="work-background">
        <div className="work">
          <p className="education-title">Work History</p>
          <p className="education-description">
            As a platform developer at IBM, I was able to gain professional
            experience in my current department as well as in my two previous
            departments
          </p>
          <ul className="steps stepss is-vertical">
            <li className="steps-segment stepss-segment">
              <span href="#" className="steps-marker stepss-marker"></span>
              <div className="steps-content stepss-content">
                <div className="box mb-5 columns">
                  <div className="ml-6 column is-3">
                    <p className="edu-title">SETS Team</p>
                    <div className="d-f fd-r mt-5 f-ac">
                      <p className="education-role">IT Apprentice</p>
                      <p className="time-span">Aug 2021 - Sep 2022</p>
                    </div>
                  </div>
                  <div className="linie-vertikal"></div>
                  <div className="graduation column is-7">
                    <p className="graduation-title">Successfully completed</p>
                    <p className="graduation-description">
                      In the Smart Education Team Switzerland (SETS) Department
                      I learned basics of IT like simple support and
                    </p>
                  </div>
                </div>
              </div>
            </li>
            <li className="steps-segment stepss-segment">
              <span href="#" className="steps-marker stepss-marker"></span>
              <div className="steps-content stepss-content">
                <div className="box mb-5 columns">
                  <div className="ml-6 column is-3">
                    <p className="edu-title">Student Cloud Lab</p>
                    <div className="d-f fd-r mt-5 f-ac">
                      <p className="education-role">IT Apprentice</p>
                      <p className="time-span">Sep 2022 - Today</p>
                    </div>
                  </div>
                  <div className="linie-vertikal"></div>
                  <div className="graduation column is-7">
                    <p className="graduation-title">Successfully completed</p>
                    <p className="graduation-description">
                      In the student cloud lab I have learned the basics of the
                      language Javascript (NodeJS), the react frontend framework
                      & the redux backend framework.
                    </p>
                  </div>
                </div>
              </div>
            </li>
            <li className="steps-segment">
              <span className="steps-marker stepss-marker"></span>
              <div className="steps-content stepss-content">
                <div className="box letzte columns">
                  <div className="ml-6 column is-3">
                    <p className="edu-title">Sustainability Data Exchange</p>
                    <div className="d-f fd-r mt-5 f-ac">
                      <p className="education-role">Student</p>
                      <p className="time-span">Mar 2024 - Today</p>
                    </div>
                  </div>
                  <div className="linie-vertikal"></div>
                  <div className="graduation column is-7">
                    <p className="graduation-title">Current Department</p>
                    <p className="graduation-description">
                      In my current department I assist in setting up OpenShift
                      cluster demos for clients, equipped with tools for B2B
                      interactions, demonstrating their capabilities and
                      benefits for improving business operations.
                    </p>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <div className="contact-background">
        <div className="contact">
          <div className="box contacts">
            <div className="hero-body">
              <div className="container has-text-centered">
                <div className="columns is-8 is-variable ">
                  <div className="column is-half">
                    <Mailbox className="mailbox" />
                  </div>
                  <div className="column is-half  ml-5">
                    <h1 className="title is-1">Contact Me</h1>

                    <div className="column has-text-left">
                      <form ref={form} onSubmit={sendEmail}>
                        <div className="field">
                          <label className="label">Name</label>
                          <div className="control has-icons-left">
                            <input
                              className="input is-medium"
                              type="text"
                              name="user_name"
                            />
                            <span className="icon is-small is-left">
                              <i className="fa-solid fa-user"></i>
                            </span>
                          </div>
                        </div>
                        <div className="field">
                          <label className="label">Email</label>
                          <div className="control has-icons-left">
                            <input
                              className="input is-medium"
                              type="email"
                              name="user_email"
                            />
                            <span className="icon is-left ">
                              <i className="fa-solid fa-envelope"></i>
                            </span>
                          </div>
                        </div>
                        <div className="field">
                          <label className="label">Subject</label>
                          <div className="control">
                            <input
                              className="input is-medium"
                              type="text"
                              name="user_subject"
                            />
                          </div>
                        </div>
                        <div className="field">
                          <label className="label">Message</label>
                          <div className="control">
                            <textarea
                              className="textarea is-medium"
                              name="message"
                            ></textarea>
                          </div>
                        </div>
                        <div className="control">
                          <button
                            type="submit"
                            className="button submit bcol-orange is-link is-fullwidth has-text-weight-medium is-medium"
                            value="Send"
                          >
                            Send Message
                            <i className="fas fa-paper-plane ml-3"></i>
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

/*----------------------------------------------------------------------------*/
/* EXPORTS                                                                    */
/*----------------------------------------------------------------------------*/
export default HomePage;
