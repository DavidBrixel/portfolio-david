/*----------------------------------------------------------------------------*/
/* IMPORTS                                                                    */
/*----------------------------------------------------------------------------*/

import { createSlice } from '@reduxjs/toolkit';

/*----------------------------------------------------------------------------*/
/* notificationSlice                                                          */
/*----------------------------------------------------------------------------*/

export const notificationSlice = createSlice({
  name: 'notification',
  initialState: {
    notifications: [],
  },
  reducers: {
    addSuccessNotification: (state, { payload }) => {
      state.notifications.push({
        kind: 'success',
        iconDescription: 'close',
        subtitle: payload.subtitle,
        title: payload.title,
      });
    },
    addErrorNotification: (state, { payload }) => {
      state.notifications.push({
        kind: 'error',
        iconDescription: 'close',
        subtitle: payload.subtitle,
        title: payload.title,
      });
    },
  },
});

/*----------------------------------------------------------------------------*/
/* EXPORTS                                                                    */
/*----------------------------------------------------------------------------*/
export const { addSuccessNotification, addErrorNotification } =
  notificationSlice.actions;

export default notificationSlice.reducer;
